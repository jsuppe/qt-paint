#include "sizepickdialog.h"
#include "ui_sizepickdialog.h"

SizePickDialog::SizePickDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SizePickDialog)
{
    ui->setupUi(this);
}

SizePickDialog::~SizePickDialog()
{
    delete ui;
}

void SizePickDialog::setBrushSize(const int size){
    m_size = size;
    ui->spinBox->setValue( m_size);
}

int SizePickDialog::getBrushSize(){
    return m_size;
}

void SizePickDialog::on_spinBox_valueChanged(int arg1)
{
    m_size = arg1;
}

