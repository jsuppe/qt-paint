#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    QCoreApplication::setOrganizationName("haxs");
    QCoreApplication::setOrganizationDomain("haxs.io");
    QCoreApplication::setApplicationName("draw");

    w.show();

    return a.exec();
}
