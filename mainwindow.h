#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindowPrivate;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void colorPickTriggered();
    void clear();

    void sizePickTriggered();

//    void openTriggered();
//    void saveTriggered();
//    void saveAsTriggered();
private:
    MainWindowPrivate *d;
};

#endif // MAINWINDOW_H
