#include "drawwidget.h"
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QRgb>
#include <QDebug>
#include <QPainterPath>

static void clearCanvas(QImage &canvas, int width, int height)
{
    canvas = QImage(width,height,QImage::Format_RGB888);
    canvas.fill(QColor(Qt::white));
}

DrawWidget::DrawWidget(QWidget *parent) : QWidget(parent)
{
    m_drawColor = QColor(Qt::black);
    clearCanvas(m_canvas, width(), height());
}

DrawWidget::~DrawWidget()
{

}

void DrawWidget::drawPixel(const QPoint pt){
    QRgb value = m_drawColor.rgb();

    QPainterPath ctx;

    ctx.moveTo(m_last.x(), m_last.y());
    ctx.lineTo(pt.x(), pt.y());

    QPainter painter(&m_canvas);
    painter.setBrush(m_drawColor);
    painter.setPen(QPen(m_drawColor, m_brushSize, Qt::SolidLine,
                        Qt::RoundCap, Qt::RoundJoin));
    painter.drawPath(ctx);
    painter.end();
    m_last = pt;

}

void DrawWidget::setBrushSize(const int size)
{
    qDebug() << "setBrushSize: " << size;
    m_brushSize = size;
}

const int DrawWidget::brushSize() const{
    return m_brushSize;
}

void DrawWidget::mousePressEvent(QMouseEvent *event){
    if(event->buttons() & Qt::LeftButton){
        m_last = event->pos();
    }
}

void DrawWidget::mouseMoveEvent(QMouseEvent *event){
    if(event->buttons() & Qt::LeftButton){
        drawPixel(event->pos());
        repaint();
    }
}

void DrawWidget::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    QImage newCanvas;
    clearCanvas(newCanvas, width(), height());

    QPainter p(&newCanvas);
    p.drawImage(0,0,m_canvas);

    m_canvas = newCanvas;

    update();
}

QColor DrawWidget::drawColor()
{
    return m_drawColor;
}

void DrawWidget::setBrushColor(QColor color)
{
    m_drawColor = color;
}

void DrawWidget::clear()
{
    clearCanvas(m_canvas, width(), height());
    update();
}

void DrawWidget::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);
    QPainter painter(this);

    painter.drawPixmap(0,0,QPixmap::fromImage(m_canvas));
}

