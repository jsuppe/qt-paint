#ifndef DRAWWIDGET_H
#define DRAWWIDGET_H

#include <QObject>
#include <QWidget>

class QPaintEvent;
class QMouseEvent;
class DrawWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DrawWidget(QWidget *parent = 0);
    ~DrawWidget();

    void drawPixel(const QPoint pt);
    void setBrushSize(const int size);
    const int brushSize() const;

public slots:
    void clear();
    QColor drawColor();
    void setBrushColor(QColor color);

signals:

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void resizeEvent(QResizeEvent *);

private:
    QColor m_drawColor;
    QImage m_canvas;
    QPoint m_last;
    int m_brushSize;
};

#endif // DRAWWIDGET_H
