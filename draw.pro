#-------------------------------------------------
#
# Project created by QtCreator 2015-03-11T10:38:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = draw
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    drawwidget.cpp \
    sizepickdialog.cpp

HEADERS  += mainwindow.h \
    drawwidget.h \
    sizepickdialog.h

FORMS    += mainwindow.ui \
    sizepickdialog.ui
