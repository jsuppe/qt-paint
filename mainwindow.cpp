#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "drawwidget.h"
#include <QColorDialog>
#include <QLayout>
#include "sizepickdialog.h"

#include <QDebug>

class MainWindowPrivate {
public:
    MainWindowPrivate(MainWindow *owner);
    ~MainWindowPrivate();

    void pickColor();
    void clear();
    void pickSize();

private:
    DrawWidget *m_drawWidget;
    MainWindow *m_Owner;
    Ui::MainWindow *m_ui;
};

MainWindowPrivate::MainWindowPrivate(MainWindow *owner) :
    m_Owner(owner),
    m_drawWidget(new DrawWidget(owner)),
    m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(m_Owner);
    m_ui->centralWidget->setLayout(new QVBoxLayout(m_Owner));
    m_ui->centralWidget->layout()->addWidget(m_drawWidget);
}

void MainWindowPrivate::pickColor()
{
    QColor color = QColorDialog::getColor(
                m_drawWidget->drawColor(),
                m_Owner,
                QString("Select a draw color"),
                QColorDialog::ShowAlphaChannel
                );
    m_drawWidget->setBrushColor(color);
}

void MainWindowPrivate::pickSize()
{
    qDebug() << "pickSize";
    auto spd = new SizePickDialog();
    spd->setBrushSize(m_drawWidget->brushSize());
    spd->exec();

    auto result = spd->result();

    int brushSize;

    switch(result){
        case QDialog::Accepted:
            brushSize = spd->getBrushSize();
            m_drawWidget->setBrushSize(brushSize);
            qDebug() << "Set brushsize : " << brushSize;
            break;
        default:
            break;
    }

}

void MainWindowPrivate::clear()
{
    m_drawWidget->clear();
}

MainWindowPrivate::~MainWindowPrivate(){
    delete m_ui;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    d(new MainWindowPrivate(this))
{

}

MainWindow::~MainWindow()
{
    delete d;
}

void MainWindow::colorPickTriggered()
{
    d->pickColor();
}

void MainWindow::sizePickTriggered()
{
    d->pickSize();
}

void MainWindow::clear()
{
    d->clear();
}
