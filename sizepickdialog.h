#ifndef SIZEPICKDIALOG_H
#define SIZEPICKDIALOG_H

#include <QDialog>

namespace Ui {
class SizePickDialog;
}

class SizePickDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SizePickDialog(QWidget *parent = nullptr);
    ~SizePickDialog();

    void setBrushSize(const int size);
    int getBrushSize();
private slots:
    void on_spinBox_valueChanged(int arg1);

private:
    Ui::SizePickDialog *ui;
    int m_size;
};

#endif // SIZEPICKDIALOG_H
